# statsd-client

statsd-client is a client library for statsd written in Go.  This code is a
fork of [that repository](https://github.com/cyberdelia/statsd).

## Installation

Download and install :

```
go get gitlab.torproject.org/tpo/anti-censorship/statsd-client
```

Add it to your code:

```go
import statsd "gitlab.torproject.org/tpo/anti-censorship/statsd-client"
```

## Usage

```go
c := statsd.Dial("localhost:8125")
c.Inc("incr", 1)
c.DecSample("decr", 1, 0.1)
c.TimingSample("timer", 320, 0.1)
c.TimeSample("timer", 0.1, func() {
        // do something  
})
c.Gauge("gauge", 30)
c.Unique("unique", 765)
```
